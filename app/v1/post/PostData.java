package v1.post;

import javax.persistence.*;

/**
 * Data returned from the database
 */
@Entity
@Table(name = "posts")
public class PostData {

    public PostData() {
    }

    public PostData(String title, String body) {
        this.title = title;
        this.body = body;
        this.name = "Anonymous";
    }

    public PostData(String title, String body, String name) {
        this.title = title;
        this.body = body;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;
    public String title;
    public String body;
    public String name;
}
