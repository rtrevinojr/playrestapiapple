package v1.post;

/**
 * Resource for the API.  This is a presentation class for frontend work.
 */
public class PostResource {
    private String id;
    private String link;
    private String title;
    private String body;
    private String name;

    public PostResource() {
    }

    public PostResource(String id, String link, String title, String body) {
        this.id = id;
        this.link = link;
        this.title = title;
        this.body = body;
        this.name = "Anonymous";
    }

    public PostResource(String id, String link, String title, String body, String name) {
        this.id = id;
        this.link = link;
        this.title = title;
        this.body = body;
        this.name = name;
    }

    public PostResource(PostData data, String link) {
        this.id = data.id.toString();
        this.link = link;
        this.title = data.title;
        this.body = data.body;
        this.name = data.name;
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getName() { return name; }

}
