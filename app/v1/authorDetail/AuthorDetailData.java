package v1.authorDetail;

import v1.author.AuthorData;
import v1.post.PostData;

public class AuthorDetailData {

    private String id;
    private String name;
    private String aboutme;
    private String title;
    private String body;

    public AuthorDetailData() {
        this.id = "0";
        this.name = "Anonymous";
        this.aboutme = "N/A";
        this.title = title;
        this.body = body;
    }

    public AuthorDetailData(String id, String name, String aboutme) {
        this.id = id;
        this.name = name;
        this.aboutme = aboutme;
    }

    public AuthorDetailData(AuthorData data) {
        if (data.id != null) this.id = data.id.toString();
        this.name = data.name;
        this.aboutme = data.aboutme;
    }

    public AuthorDetailData setAuthorData (AuthorData authorData) {

        this.name = authorData.name;
        this.aboutme = authorData.aboutme;
        return this;
    }

    public AuthorDetailData setPostData (PostData postData) {
        this.title = postData.title;
        this.body = postData.body;
        return this;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAboutMe() {
        return aboutme;
    }


}
