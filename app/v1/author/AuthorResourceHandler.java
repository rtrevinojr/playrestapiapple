package v1.author;

import play.libs.concurrent.HttpExecutionContext;
import v1.post.PostData;
import v1.post.PostResource;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;


public class AuthorResourceHandler {
	
	
    private final AuthorRepository repository;
    private final HttpExecutionContext ec;

    @Inject
    public AuthorResourceHandler(AuthorRepository repository, HttpExecutionContext ec) {
        this.repository = repository;
        this.ec = ec;
    }

    public CompletionStage<Stream<AuthorResource>> find() {
        return repository.list().thenApplyAsync(authorDataStream -> {
            return authorDataStream.map(data -> new AuthorResource(data));
        }, ec.current());
    }

    public CompletionStage<AuthorResource> create(AuthorResource resource) {

        final AuthorData data = new AuthorData(resource.getName(), resource.getAboutme());
        return repository.create(data).thenApplyAsync(savedData -> {
            return new AuthorResource(savedData);
        }, ec.current());
        
    }

    public CompletionStage<Optional<AuthorResource>> lookup(String id) {
        return repository.get(Long.parseLong(id)).thenApplyAsync(optionalData -> {
            return optionalData.map(data -> new AuthorResource(data));
        }, ec.current());
    }

    public CompletionStage<Optional<AuthorResource>> lookupByName(String name) {
        return repository.get(name).thenApplyAsync(optionalData -> {
            return optionalData.map(data -> new AuthorResource(data));
        }, ec.current());
    }

    public CompletionStage<Optional<AuthorResource>> update(String id, AuthorResource resource) {
        
        final AuthorData data = new AuthorData(resource.getName(), resource.getAboutme());

        return repository.update(Long.parseLong(id), data).thenApplyAsync(optionalData -> {

            return optionalData.map(op -> new AuthorResource(op));

        }, ec.current());

    }

}



