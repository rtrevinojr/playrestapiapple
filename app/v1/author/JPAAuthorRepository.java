package v1.author;

import static java.util.concurrent.CompletableFuture.supplyAsync;

import net.jodah.failsafe.CircuitBreaker;
import net.jodah.failsafe.Failsafe;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import java.util.*;


/**
 * A repository that provides a non-blocking API with a custom execution context
 * and circuit breaker.
 */
@Singleton
public class JPAAuthorRepository implements AuthorRepository {
	
	
    private final JPAApi jpaApi;
    private final AuthorExecutionContext ec;
    private final CircuitBreaker circuitBreaker = new CircuitBreaker().withFailureThreshold(1).withSuccessThreshold(3);

    @Inject
    public JPAAuthorRepository(JPAApi api, AuthorExecutionContext ec) {
        this.jpaApi = api;
        this.ec = ec;
    }

    @Override
    public CompletionStage<Stream<AuthorData>> list() {
        return supplyAsync(() -> wrap(em -> select(em)), ec);
    }

    @Override
    public CompletionStage<AuthorData> create(AuthorData authorData) {

        return supplyAsync(() -> wrap(em -> lookupByName(em, authorData.name)), ec)
                .thenApplyAsync(author -> author.orElseGet(() -> wrap(em -> insert(em, authorData))), ec);

    }

    @Override
    public CompletionStage<Optional<AuthorData>> get(Long id) {
        return supplyAsync(() -> wrap(em -> lookup(em, id)), ec);
    }

    public CompletionStage<Optional<AuthorData>> get(String name) {
        return supplyAsync(() -> wrap(em -> lookupByName(em, name)), ec);
    }

    @Override
    public CompletionStage<Optional<AuthorData>> update(Long id, AuthorData authorData) {
        return supplyAsync(() -> wrap(em -> modify(em, id, authorData)), ec);
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private Optional<AuthorData> lookup(EntityManager em, Long id) {

        //throw new SQLException("Call this to cause the circuit breaker to trip");
        return Optional.ofNullable(em.find(AuthorData.class, id));
    }

    private Optional<AuthorData> lookupByName(EntityManager em, String name) {

        TypedQuery<AuthorData> query = em.createQuery("SELECT a FROM AuthorData a WHERE name = :name", AuthorData.class);
        query.setParameter("name", name);
        List <AuthorData> result = query.getResultList();
        if (!result.isEmpty()) return Optional.of(result.get(0));
        return Optional.empty();
    }

    private Stream<AuthorData> select(EntityManager em) {
        TypedQuery<AuthorData> query = em.createQuery("SELECT a FROM AuthorData a", AuthorData.class);
        return query.getResultList().stream();
    }

    private Optional<AuthorData> modify(EntityManager em, Long id, AuthorData authorData)  {
        final AuthorData data = em.find(AuthorData.class, id);
        if (data != null) {
        	
        	//data.name = authorData.name;
        	data.aboutme = authorData.aboutme;
        }
        //Thread.sleep(10000L);
        return Optional.ofNullable(data);
    }

    private AuthorData insert(EntityManager em, AuthorData authorData) {
    	
        return em.merge(authorData);
    }

}
