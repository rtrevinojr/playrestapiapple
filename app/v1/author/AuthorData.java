package v1.author;//package v1.author;


import javax.persistence.*;


/**
 * Data returned from the database
 */
@Entity
@Table(name = "authors")
public class AuthorData {
	
	public AuthorData () {
		
		this.name = "Anonymous";
		this.aboutme = "N/A";
	}
	

	public AuthorData (String name, String aboutme) {
		
		this.name = name;
		this.aboutme = aboutme;
		
	}
	
	public AuthorData (Long id, String name, String aboutme) {
		
		this.id = id;
		this.name = name;
		this.aboutme = aboutme;
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    public String name;
    public String aboutme;
    
}

