package v1.author;

public class AuthorResource {
	
    private String id;
    private String name;
    private String aboutme;

    public AuthorResource() {
    	this.id = "0";
    	this.name = "Anonymous";
    	this.aboutme = "N/A";
    }

    public AuthorResource(String id, String name, String aboutme) {
        this.id = id;
        this.name = name;
        this.aboutme = aboutme;
    }

    public AuthorResource(AuthorData data) {
        if (data.id != null) this.id = data.id.toString();
        this.name = data.name;
        this.aboutme = data.aboutme;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAboutme() {
        return aboutme;
    }

    
}


