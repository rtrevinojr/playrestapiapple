

package it;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import v1.author.AuthorData;
import v1.author.AuthorRepository;
import v1.author.AuthorResource;


import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static play.test.Helpers.*;


public class AuthorIntegrationTest extends WithApplication {
	

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }
    
    

    @Test
    public void testList() {
    	AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM!"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/authors");

        Result result = route(app, request);
        final String body = contentAsString(result);
        assertThat(body, containsString("Computer Scientist at GM!"));
    }


    @Test
    public void testGet() {
        AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM!"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/authors");

        Result result = route(app, request);
        final String body = contentAsString(result);
        assertThat(body, containsString("Computer Scientist at GM!"));
    }


    @Test
    public void testPut() {
        AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(PUT)
                .uri("/v1/authors/?name=hector&aboutme=cs");

        Result result = route(app, request);
        final String body = contentAsString(result);
        assertThat(body, containsString("cs"));
    }

    @Test
    public void testPost() {
        AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/v1/authors/1/?name=hector&aboutme=cs");

        Result result = route(app, request);
        final String body = contentAsString(result);
        assertThat(body, containsString("cs"));
    }


    /*
    @Test
    public void testTimeoutOnUpdate() {
    	AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM!"));

        JsonNode json = Json.toJson(new AuthorResource("1", "Hector Trevino", "Computer Scientist at GM!"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(PUT)
                .bodyJson(json)
                .uri("/v1/authors/100000000000000000");

        Result result = route(app, request);
        assertThat(result.status(), equalTo(GATEWAY_TIMEOUT));
    }
*/


/*

    @Test
    public void testCircuitBreakerOnShow() {
    	AuthorRepository repository = app.injector().instanceOf(AuthorRepository.class);
        repository.create(new AuthorData("Hector Trevino", "Computer Scientist at GM!"));

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(G)
                .uri("/v1/posts/bad");

        Result result = route(app, request);
        assertThat(result.status(), equalTo(SERVICE_UNAVAILABLE));
    }*/


}


